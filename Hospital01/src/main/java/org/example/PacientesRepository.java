package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PacientesRepository {
    private final static String URL = "jdbc:postgresql://localhost:5432/postgres";
    private final static String USERNAME = "postgres";
    private final static String PASSWORD = "postgres";

    public void insertMessage(final Pacientes pacientes) {
        Connection connection = HospitalFactory.getconnection();
        PreparedStatement preparedStatement = null;
        try {
            String sql = "insert into pacientes values(?,?,?,?)";
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, pacientes.getId());
            preparedStatement.setString(2, pacientes.getNome());
            preparedStatement.setInt(3, pacientes.getTelefone());
            preparedStatement.setDate(4, pacientes.getNascimento());
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
    public void update(Pacientes pacientes){
        String sql = "update pacientes set nome = ? set telefone = ? set nascimento = ? where id = ?";
        PreparedStatement preparedStatement = null;
        try(Connection connection = HospitalFactory.getconnection()){
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, pacientes.getId());
            preparedStatement.setString(2, pacientes.getNome());
            preparedStatement.setInt(3, pacientes.getTelefone());
            preparedStatement.setDate(4, pacientes.getNascimento());
            preparedStatement.execute();
            preparedStatement.close();
        } catch(SQLException e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    public void delete(int id){
        String sql = "delete from pacientes where id = ?";
        PreparedStatement preparedStatement = null;
        try(Connection connection = HospitalFactory.getconnection()){
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            preparedStatement.close();
        } catch(SQLException e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
