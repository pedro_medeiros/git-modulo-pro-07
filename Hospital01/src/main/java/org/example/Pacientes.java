package org.example;


import java.sql.Date;

public class Pacientes {
    private int id;
    private String nome;
    private int telefone;
    private Date nascimento;

    public Pacientes(int id, String nome, int telefone, Date nascimento) {
        this.id = id;
        this.nome = nome;
        this.telefone = telefone;
        this.nascimento = nascimento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getTelefone() {
        return telefone;
    }

    public void setTelefone(int telefone) {
        this.telefone = telefone;
    }

    public Date getNascimento() {
        return nascimento;
    }

    public void setNascimento(Date nascimento) {
        this.nascimento = nascimento;
    }
}
