package org.example;

import org.postgresql.core.ConnectionFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class HospitalRepository {

    private final static String URL = "jdbc:postgresql://localhost:5432/postgres";
    private final static String USERNAME = "postgres";
    private final static String PASSWORD = "postgres";

    public void insertMessage(final Profissionais profissionais){
        Connection connection = HospitalFactory.getconnection();
        PreparedStatement preparedStatement = null;
        try{
            String sql = "insert into profissionais values(?,?,?,?,?)";
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, profissionais.getMatrícula());
            preparedStatement.setString(2, profissionais.getNome());
            preparedStatement.setString(3, profissionais.getDepartamento());
            preparedStatement.setString(4, profissionais.getCargo());
            preparedStatement.setInt(5, profissionais.getTelefone());
            preparedStatement.execute();
        } catch(SQLException throwables){
            throwables.printStackTrace();
        }finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
    public void update(Profissionais profissionais){
        String sql = "update profissionais set nome = ? set departamento = ? set cargo = ? set telefone = ? where matrícula = ?";
        PreparedStatement preparedStatement = null;
        try(Connection connection = HospitalFactory.getconnection()){
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, profissionais.getMatrícula());
            preparedStatement.setString(2, profissionais.getNome());
            preparedStatement.setString(3, profissionais.getDepartamento());
            preparedStatement.setString(4, profissionais.getCargo());
            preparedStatement.setInt(5, profissionais.getTelefone());
            preparedStatement.execute();
            preparedStatement.close();
        } catch(SQLException e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }
    public void delete(long matrícula){
        String sql = "delete from profissionais where matrícula = ?";
        PreparedStatement preparedStatement = null;
        try(Connection connection = HospitalFactory.getconnection()){
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, matrícula);
            preparedStatement.execute();
            preparedStatement.close();
        } catch(SQLException e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

}
