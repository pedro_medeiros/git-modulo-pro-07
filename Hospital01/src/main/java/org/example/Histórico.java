package org.example;


import java.sql.Date;

public class Histórico {
    private int id;
    private Date data_de_entrada_do_paciente;
    private Date data_de_saída_do_paciente;
    private String diagnóstico;
    private String médico_responsável;

    public Histórico(int id, Date data_de_entrada_do_paciente, Date data_de_saída_do_paciente, String diagnóstico, String médico_responsável) {
        this.id = id;
        this.data_de_entrada_do_paciente = data_de_entrada_do_paciente;
        this.data_de_saída_do_paciente = data_de_saída_do_paciente;
        this.diagnóstico = diagnóstico;
        this.médico_responsável = médico_responsável;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getData_de_entrada_do_paciente() {
        return data_de_entrada_do_paciente;
    }

    public void setData_de_entrada_do_paciente(Date data_de_entrada_do_paciente) {
        this.data_de_entrada_do_paciente = data_de_entrada_do_paciente;
    }

    public Date getData_de_saída_do_paciente() {
        return data_de_saída_do_paciente;
    }

    public void setData_de_saída_do_paciente(Date data_de_saída_do_paciente) {
        this.data_de_saída_do_paciente = data_de_saída_do_paciente;
    }

    public String getDiagnóstico() {
        return diagnóstico;
    }

    public void setDiagnóstico(String diagnóstico) {
        this.diagnóstico = diagnóstico;
    }

    public String getMédico_responsável() {
        return médico_responsável;
    }

    public void setMédico_responsável(String médico_responsável) {
        this.médico_responsável = médico_responsável;
    }
}
