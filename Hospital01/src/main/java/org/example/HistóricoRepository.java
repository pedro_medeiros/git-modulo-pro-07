package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class HistóricoRepository {
    private final static String URL = "jdbc:postgresql://localhost:5432/postgres";
    private final static String USERNAME = "postgres";
    private final static String PASSWORD = "postgres";

    public void insertMessage(final Histórico histórico){
        Connection connection = HospitalFactory.getconnection();
        PreparedStatement preparedStatement = null;
        try{
            String sql = "insert into histórico values(?,?,?,?,?)";
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, histórico.getId());
            preparedStatement.setDate(2, histórico.getData_de_entrada_do_paciente());
            preparedStatement.setDate(3, histórico.getData_de_saída_do_paciente());
            preparedStatement.setString(4, histórico.getDiagnóstico());
            preparedStatement.setString(5, histórico.getMédico_responsável());
            preparedStatement.execute();
        } catch(SQLException throwables){
            throwables.printStackTrace();
        }finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
    public void update(Histórico histórico){
        String sql = "update histórico set data_de_entrada_do_paciente = ? set data_de_saída_do_paciente = ? set diagnóstico = ? set médico_responsável = ? where id = ?";
        PreparedStatement preparedStatement = null;
        try(Connection connection = HospitalFactory.getconnection()){
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, histórico.getId());
            preparedStatement.setDate(2, histórico.getData_de_entrada_do_paciente());
            preparedStatement.setDate(3, histórico.getData_de_saída_do_paciente());
            preparedStatement.setString(4, histórico.getDiagnóstico());
            preparedStatement.setString(5, histórico.getMédico_responsável());
            preparedStatement.execute();
            preparedStatement.close();
        } catch(SQLException e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }
    public void delete(int id){
        String sql = "delete from histórico where id = ?";
        PreparedStatement preparedStatement = null;
        try(Connection connection = HospitalFactory.getconnection()){
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
            preparedStatement.close();
        } catch(SQLException e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
