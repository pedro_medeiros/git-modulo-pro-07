package org.example;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        HospitalRepository hospitalRepository = new HospitalRepository();
        hospitalRepository.insertMessage(new Profissionais(648341, "Bianca", "Geriatria", "Geriatra", 90667442));
        hospitalRepository.update(new Profissionais(647594, "Jonathan", "Geriatria", "Geriatra", 76934275));
        hospitalRepository.delete(648341);
    }
}
